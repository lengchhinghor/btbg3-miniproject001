package com.example.jdbc_demo.controller;

import com.example.jdbc_demo.model.Employee;
import com.example.jdbc_demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@RestController
@RequestMapping("/api/v1/employee")
public class StudentRestController {

//    private EmployeeRepository employeeRepository;
      private EmployeeService employeeService;
    @Autowired
    public StudentRestController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
    @GetMapping
    public List<Employee> findAll(){
        return employeeService.findAll();
    }
    @GetMapping("/{id}")
    public Employee findById(@PathVariable int id){
        return employeeService.findOne(id);
    }
    @PutMapping("/{id}")
    public Employee update(@PathVariable int id, @RequestBody Employee employee){
        if(employeeService.update(id,employee)){
            return employeeService.findOne(id);
        }
        else
            return null;
    }
    @DeleteMapping("/{id}")
    public Serializable delete(@PathVariable int id){
        if(employeeService.delete(id)){
            return "Data Has been delete";
        }
        else
            return "No record found";

    }
    @PostMapping("/create")
    public String insert(@RequestBody Employee employee){
        if(employeeService.saveEmployee(employee)) {
            return "Dat has been created";
        }
        else
            return "Fail created";
    }

}
