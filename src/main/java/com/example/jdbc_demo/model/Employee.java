package com.example.jdbc_demo.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Employee {
    private int id;
    private String name;
    private String gender;
}
