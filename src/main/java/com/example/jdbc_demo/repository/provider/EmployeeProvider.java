package com.example.jdbc_demo.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class EmployeeProvider {
    String tablename ="employee";
    public String getAllEmployee(){
        return new SQL(){
            {
                SELECT("*");
//                FROM("employee"); //way 1
                FROM(tablename);
            }
        }.toString();
    }
}
