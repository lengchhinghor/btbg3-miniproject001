package com.example.jdbc_demo.repository;

import com.example.jdbc_demo.model.Employee;
import com.example.jdbc_demo.repository.provider.EmployeeProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface EmployeeRepository {

//    to get function from provider
    @SelectProvider(type = EmployeeProvider.class, method = "getAllEmployee")
    public List<Employee> findAll();
//    raw query by repository not related with provider
//    @Select("select * from employee")
//    Case changed colome name
//    @Result(
//            @Result(property = "profile", column = "avatar")
//    )
//    public List<Employee> findAll();

    @Select("select * from employee where id = #{id}")
    public Employee findOne(@Param("id") int id);

    @Update("update employee set name=#{employee.name}, gender= #{employee.gender} where id =#{id}")
    public Boolean update( @Param("id") int id, @Param("employee") Employee employee);

    @Delete("delete from employee where id = #{id}")
    public Boolean delete(@Param("id") int id);

    @Insert("insert into employee (id,name,gender) values (#{employee.id},#{employee.name},#{employee.gender})")
    public  Boolean insert(@Param("employee") Employee employee);
}

