package com.example.jdbc_demo.service.imp;

import com.example.jdbc_demo.model.Employee;
import com.example.jdbc_demo.repository.EmployeeRepository;
import com.example.jdbc_demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImp implements EmployeeService {
    private EmployeeRepository employeeRepository;
    @Autowired
    public EmployeeServiceImp(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee findOne(int id) {
        return employeeRepository.findOne(id);
    }

    @Override
    public Boolean update(int id, Employee employee) {
        return employeeRepository.update(id, employee);
    }

    @Override
    public Boolean delete(int id) {
        return employeeRepository.delete(id);
    }

    @Override
    public Boolean saveEmployee(Employee employee) {
        return employeeRepository.insert(employee);
    }


}
