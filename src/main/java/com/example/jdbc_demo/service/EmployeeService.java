package com.example.jdbc_demo.service;

import com.example.jdbc_demo.model.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeService {
//service to get all
List<Employee> findAll();
//service to get by id
Employee findOne(@Param("id") int id);
// update
Boolean update(@Param("id") int id, Employee employee);
Boolean delete(@Param("id") int id);
Boolean saveEmployee(Employee employee);
}
